package maum.brain.tts;

public class CommonCode {

    //Socket related error
    public static int TTS_ERR_CONN_REFUSED  = 50001;
    public static int TTS_ERR_CONN_TIMEOUT  = 50002;
    public static int TTS_ERR_REQ_TIMEOUT   = 50003;

    public static int TTS_ERR_TACOTRON_ERROR    = 51001;
    public static int TTS_ERR_WAVENET_ERROR     = 52001;

    public static int TTS_ERR_AUTH_VERIFICATION_ERROR   = 40001;
    public static int TTS_ERR_SERVER_NOT_FOUND_ERROR    = 40002;
    public static int TTS_ERR_NOT_SUPPORTED_LANG_ERROR    = 40003;

    //Service related error
    public static int TTS_ERR_SVC_ERROR = 99999;
    public static int TTS_ERR_SVC_EXCEEDED_TEXT_LENGTH = 60001;
    public static String TTS_ERR_MSG_SVC_EXCEEDED_TEXT_LENGTH = "Requested text exceeded max. length.";
    public static int TTS_ERR_SVC_EXCEEDED_SPEAKER = 60002;
    public static String TTS_ERR_MSG_SVC_EXCEEDED_SPEAKER = "Requested speaker exceeded max. index.";
    public static int TTS_ERR_SVC_WRONG_AUDIO_ENCODING = 60003;
    public static String TTS_ERR_MSG_WRONG_AUDIO_ENCODING = "Requested audioEncoding was wrong.";

    public enum JOB_STATUS{
        notexist,
        started,
        running,
        completed,
        error
    }
}
