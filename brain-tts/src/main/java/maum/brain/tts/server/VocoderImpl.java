package maum.brain.tts.server;

import io.grpc.stub.StreamObserver;
import maum.brain.tts.Tts;
import maum.brain.tts.VocoderGrpc.VocoderImplBase;
import org.springframework.stereotype.Component;

@Component
public class VocoderImpl extends VocoderImplBase {
    @Override
    public void mel2Wav(Tts.MelSpectrogram request, StreamObserver<Tts.WavData> responseObserver) {
        super.mel2Wav(request, responseObserver);
    }
}
