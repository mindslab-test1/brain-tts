package maum.brain.tts;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.health.v1.HealthCheckResponse;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.protobuf.services.HealthStatusManager;
import maum.brain.tts.server.NgTtsServer;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@Import({TTSConfig.class})
public class Application implements CommandLineRunner {
    private static Logger logger = LoggerFactory.getLogger(Application.class);

    private Server server;

    @Value("${tts.port}")
    private int port;

    @Value("${tts.thread}")
    private int nThreads;

    @Autowired
    private NgTtsServer ngTtsServer;

    public static void main(String[] args) {
        logger.info("tts server started.");

        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        // create the Options
//        Options options = new Options();
//        Option help = new Option( "help", "help" );
//
//        options.addOption(help);
//        options.addOption("port", true, "server port");
//        options.addOption("maxlength", true, "max text length");
//
//        try {
//            CommandLineParser parser = new DefaultParser();
//            // parse the command line arguments
//            CommandLine line = parser.parse(options, args);
//
//            if (line.hasOption("help")) {
//                printUsage(options);
//                System.exit(0);
//            }
//
////            Application app = new Application();
//            if (line.hasOption("port")) {
//                String s = line.getOptionValue("port");
//                int p = 0;
//                if(s.matches("\\d+")){
//                    p = Integer.parseInt(s);
//                    this.setPort(p);
//                }else{
//                    logger.error("port parsing error. " + s);
//                    printUsage(options);
//                    System.exit(-1);
//                }
//
//            }else{
//                printUsage(options);
//                logger.error("port is not specified.");
//                System.exit(-1);
//            }
//
//            try {
//                this.start(this.getPort());
//                this.blockUntilShutdown();
//            } catch (IOException | InterruptedException e) {
//                e.printStackTrace();
//            }
//
//        }catch(Exception e){
//            logger.error("{}", e.getMessage());
//        }

        try {
            this.start(port);
            this.blockUntilShutdown();
        }catch(Exception e){
            logger.warn("server stopped. {}", e.getMessage());
        }
    }

    public int getPort(){
        return port;
    }

    public void setPort(int port){
        this.port = port;
    }

    private void start(int port) throws IOException {

        int grpcTimeout = 30000;

        ServerBuilder sb = NettyServerBuilder.forPort(port)
                .executor(Executors.newFixedThreadPool(nThreads))
//                .maxConnectionIdle(grpcTimeout, TimeUnit.MILLISECONDS)
//                .maxConnectionAge(grpcTimeout, TimeUnit.MILLISECONDS)
                .keepAliveTime(30, TimeUnit.SECONDS)
                ;


        //sb.addService(new NgTtsServer());
        sb.addService(ngTtsServer);

        HealthStatusManager healthStatusManager = new HealthStatusManager();
        sb.addService(healthStatusManager.getHealthService());
        server = sb.build().start();
        logger.info("TTS Server started on port {}", port);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                healthStatusManager.enterTerminalState();
                // Use stderr here since the logger may have been reset by its
                // JVM shutdown hook.
                Application.this.stop();
            }
        });
        healthStatusManager.setStatus("", HealthCheckResponse.ServingStatus.SERVING);
    }

    private void stop() {

        if (server != null) {
            server.shutdown();
            try {
                // Wait for RPCs to complete processing
                if (!server.awaitTermination(60, TimeUnit.SECONDS)) {
                    // That was plenty of time. Let's cancel the remaining RPCs
                    server.shutdownNow();
                    // shutdownNow isn't instantaneous, so give a bit of time to clean resources up
                    // gracefully. Normally this will be well under a second.
                    server.awaitTermination(5, TimeUnit.SECONDS);
                }
            } catch (InterruptedException ex) {
                server.shutdownNow();
            }
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }


    public static void printUsage(Options options){
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "help", options );
    }
}
