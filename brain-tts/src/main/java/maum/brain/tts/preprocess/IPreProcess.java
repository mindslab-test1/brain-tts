package maum.brain.tts.preprocess;

import java.io.IOException;
import java.util.ArrayList;

public interface IPreProcess {
    public ArrayList<String> preProcessText(String text) throws IOException;
}
