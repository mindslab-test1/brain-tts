package maum.brain.tts.rest.utils;

import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.Calendar;
import java.util.Date;

public class ConvertAudio {

    @Value("${tts.downloads}")
    private String filePath;

    public String convert(File inputFile,String fileSet, String speed, String tone,int samplerate){

        String cmdLine ;
        String fileName;
        String resultFileInfo="";
        try {
            String line;
            filePath = inputFile.getCanonicalPath().replace(inputFile.getName(),"");
            fileName=inputFile.getName();
            cmdLine = "ffmpeg -i "+filePath+fileName;
            if(tone.equals("Up")){
                if(speed.equals("1.2")) {
                    cmdLine += " -af asetrate="+samplerate+"*1.07,atempo=1.17,aresample="+samplerate;
                } else if(speed.equals("1.0")){
                    cmdLine += " -af asetrate="+samplerate+"*1.07,atempo=1/1.07,aresample="+samplerate;
                } else if(speed.equals("0.8")){
                    cmdLine += " -af asetrate="+samplerate+"*1.07,atempo=1/1.14,atempo=1/1.13,aresample="+samplerate;
                }
            }else if(tone.equals("Down")){
                if(speed.equals("1.2")) {
                    cmdLine += " -af asetrate="+samplerate +"*0.93,atempo=1.35,aresample="+samplerate;
                } else if(speed.equals("1.0")){
                    cmdLine += " -af asetrate="+samplerate+"*0.93,atempo=1/0.93,aresample="+samplerate;
                } else if(speed.equals("0.8")){
                    cmdLine += " -af asetrate="+samplerate+"*0.93,atempo=1/1.11,aresample="+samplerate;
                }
            } else{
                cmdLine += " -af atempo="+speed;
            }
            if (fileSet.equals("mp3")){
                cmdLine += " -ab 128k";
            }
            cmdLine += " -y "+filePath+tone+"_"+speed+"_"+fileName.replaceAll("\\..*","."+fileSet);
            fileName = tone+"_"+speed+"_"+fileName.replaceAll("\\..*","."+fileSet);
            resultFileInfo = fileName;
            Process p = Runtime.getRuntime().exec(cmdLine);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(p.getErrorStream()));
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }

            in.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return resultFileInfo;
    }


    public void deleteFile(){
        Calendar cal = Calendar.getInstance() ;
        long todayMil = cal.getTimeInMillis() ;     // 현재 시간(밀리 세컨드)
        long oneMinMil = 60*1000 ;            // 일 단위
        Calendar fileCal = Calendar.getInstance() ;
        Date fileDate = null ;
        File path = new File(filePath) ;
        File[] list = path.listFiles() ;            // 파일 리스트 가져오기
        for(int j=0 ; j < list.length; j++){
            // 파일의 마지막 수정시간 가져오기
            fileDate = new Date(list[j].lastModified()) ;

            // 현재시간과 파일 수정시간 시간차 계산(단위 : 밀리 세컨드)
            fileCal.setTime(fileDate);
            long diffMil = todayMil - fileCal.getTimeInMillis() ;

            int diffMin = (int)(diffMil/oneMinMil) ;
            //생서된지 3시간이상 파일 삭제
            if(diffMin > 180 && list[j].exists()){
                list[j].delete() ;
                System.out.println(list[j].getName() + " 파일을 삭제했습니다.");
            }

        }

    }
}
